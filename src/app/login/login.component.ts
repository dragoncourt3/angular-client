import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '@/_services';
import {Socket} from 'ngx-socket-io';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public error = '';
    public loginForm: FormGroup;
    public submitted = false;

    constructor(private formBuilder: FormBuilder,
                private router: Router,
                private authService: AuthService,
                private socket: Socket) {
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
        });

        this.socket.on('not authenticated', error => {
            this.error = error.message;
        });
    }

    public get f() {
        return this.loginForm.controls;
    }

    public onSubmit() {
        this.submitted = true;

        if (this.loginForm.invalid) {
            return;
        }

        this.socket.emit('authenticate', {
            username: this.f.username.value,
            password: this.f.password.value,
        });
    }

}
