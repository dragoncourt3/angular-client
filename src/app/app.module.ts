import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FaqComponent} from './faq/faq.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './login/login.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ErrorInterceptor} from '@/_helpers';
import {SocketIoConfig, SocketIoModule} from 'ngx-socket-io';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {GameModule} from '@/game/game.module';
import {environment} from '../environments/environment';

const socketConfig: SocketIoConfig = {url: environment.serverUrl};

@NgModule({
    declarations: [
        AppComponent,
        FaqComponent,
        LoginComponent,
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        FormsModule,
        FontAwesomeModule,
        GameModule,
        HttpClientModule,
        ReactiveFormsModule,
        SocketIoModule.forRoot(socketConfig),
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
