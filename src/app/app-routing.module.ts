import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {GameComponent} from '@/game/game.component';
import {AuthGuard} from '@/_guards/auth.guard';
import {FaqComponent} from '@/faq/faq.component';

const routes: Routes = [
    {path: '', component: GameComponent, canActivate: [AuthGuard]},
    {path: 'auth/login', component: LoginComponent},
    {path: 'faqs', component: FaqComponent},

    {path: '**', redirectTo: ''},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
