import {Component} from '@angular/core';
import {User} from '@/_models';
import {Router} from '@angular/router';
import {AuthService} from '@/_services';
import {Socket} from 'ngx-socket-io';
import {version} from '../../package.json';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    public currentUser: User;
    public version = version;

    constructor(private router: Router,
                private socket: Socket,
                private authService: AuthService) {
        this.authService.currentUser.subscribe(user => {
            this.currentUser = user;
        });

        this.socket.on('authenticated', user => {
            console.log('Authenticated', user);
            this.authService.loggedIn(user);
            this.router.navigate(['/']);
        });

        this.socket.on('not authenticated', () => this.logout());

        this.socket.on('disconnect', reason => {
            console.log('Socket disconnected', reason);
            this.socket.once('connect', () => this.authService.sendToken());
        });
    }

    public logout() {
        this.authService.logout();
        this.socket.disconnect();
        this.socket.connect();
        this.router.navigate(['/auth/login']);
    }

}
