import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '@/_models';
import {Socket} from 'ngx-socket-io';

@Injectable({providedIn: 'root'})
export class AuthService {

    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private socket: Socket) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('current_user')));
        this.currentUser = this.currentUserSubject.asObservable();

        this.sendToken();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    public sendToken() {
        if (this.currentUserValue) {
            this.socket.emit('authenticate token', this.currentUserValue.token);
        }
    }

    public loggedIn(user) {
        localStorage.setItem('current_user', JSON.stringify(user));
        this.currentUserSubject.next(user);
    }

    public logout() {
        localStorage.removeItem('current_user');
        this.currentUserSubject.next(null);
    }

}
