import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class GameStateService {

    private characterSubject: BehaviorSubject<any>;
    public characterListener: Observable<any>;

    private zoneSubject: BehaviorSubject<any>;
    public zoneListener: Observable<any>;

    constructor() {
        this.characterSubject = new BehaviorSubject<any>({});
        this.characterListener = this.characterSubject.asObservable();

        this.zoneSubject = new BehaviorSubject<any>({});
        this.zoneListener = this.zoneSubject.asObservable();
    }

    get character() {
        return this.characterSubject.value;
    }

    set character(character) {
        this.characterSubject.next(character);
    }

    get zone() {
        return this.zoneSubject.value;
    }

    set zone(zone) {
        this.zoneSubject.next(zone);
    }

}
