import {Component} from '@angular/core';
import {Shop} from '@/game/shops/shop';

@Component({
    selector: 'app-game-shop-fields-healer',
    templateUrl: './healer.component.html',
    styleUrls: ['./healer.component.scss']
})
export class HealerComponent extends Shop {

    public heal() {
        this.socket.emit('cheat full heal');
    }

    public refreshQuests() {
        this.socket.emit('cheat refresh quests');
    }

}

