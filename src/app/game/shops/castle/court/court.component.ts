import {Component} from '@angular/core';
import {Shop} from '@/game/shops/shop';

@Component({
    selector: 'app-game-shop-castle-court',
    templateUrl: './court.component.html',
    styleUrls: ['./court.component.scss']
})
export class CourtComponent extends Shop {

}

