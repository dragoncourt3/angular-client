import {Component} from '@angular/core';
import {Shop} from '@/game/shops/shop';

@Component({
    selector: 'app-game-shop-castle-guild',
    templateUrl: './guild.component.html',
    styleUrls: ['./guild.component.scss']
})
export class GuildComponent extends Shop {

    public castleGuildQuest = 0;

}

