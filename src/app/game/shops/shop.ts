import {Socket} from 'ngx-socket-io';
import {GameStateService} from '@/_services';
import {Injectable} from '@angular/core';

@Injectable()
export abstract class Shop {

    constructor(protected socket: Socket,
                protected game: GameStateService) {
    }

}
