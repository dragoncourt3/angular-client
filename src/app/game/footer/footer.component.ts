import {Component, OnInit} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {GameStateService} from '@/_services';
import {faGlobeAmericas, faWalking} from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-game-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    public icons = {
        character: faWalking,
        world: faGlobeAmericas,
    };

    constructor(private socket: Socket,
                public game: GameStateService) {
    }

    ngOnInit() {
    }

    public characterSheet() {
        if (this.game.zone.type === 'character') {
            this.socket.emit('reload zone');
        } else {
            this.socket.emit('show character');
        }
    }

}
