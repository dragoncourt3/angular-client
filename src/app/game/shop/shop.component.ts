import {Component, OnInit} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {GameStateService} from '@/_services';

@Component({
    selector: 'app-game-shop',
    templateUrl: './shop.component.html',
    styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

    constructor(private socket: Socket,
                private game: GameStateService) {
    }

    ngOnInit() {
        this.socket.on('load shop', data => {
            console.log('Loading shop', data);

            this.game.zone = data.shop;
            this.game.character = data.character;
        });
    }

    public isShop() {
        return this.game.zone.type === 'shop';
    }

    public leaveShop() {
        this.socket.emit('reload zone');
    }

}
