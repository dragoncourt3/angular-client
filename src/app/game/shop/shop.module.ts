import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ShopComponent} from '@/game/shop/shop.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GuildComponent} from '@/game/shops/castle/guild/guild.component';
import {CourtComponent} from '@/game/shops/castle/court/court.component';
import {HealerComponent} from '@/game/shops/fields/healer/healer.component';

@NgModule({
    declarations: [
        ShopComponent,
        CourtComponent,
        GuildComponent,
        HealerComponent,
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
        ShopComponent,
        CourtComponent,
        GuildComponent,
        HealerComponent,
    ],
})
export class ShopModule {
}
