import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GameComponent} from '@/game/game.component';
import {CharacterComponent} from '@/game/character/character.component';
import {EncounterComponent} from '@/game/encounter/encounter.component';
import {FooterComponent} from '@/game/footer/footer.component';
import {NotificationComponent} from '@/game/notification/notification.component';
import {ShopModule} from '@/game/shop/shop.module';
import {ZoneComponent} from '@/game/zone/zone.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    declarations: [
        GameComponent,
        CharacterComponent,
        EncounterComponent,
        FooterComponent,
        NotificationComponent,
        ZoneComponent,
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        FormsModule,
        ReactiveFormsModule,
        ShopModule,
    ],
    exports: [
        GameComponent,
        CharacterComponent,
        EncounterComponent,
        FooterComponent,
        NotificationComponent,
        ZoneComponent,
    ],
})
export class GameModule {
}
