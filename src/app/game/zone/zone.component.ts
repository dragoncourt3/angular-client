import {Component, OnInit} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {GameStateService} from '@/_services';

@Component({
    selector: 'app-game-zone',
    templateUrl: './zone.component.html',
    styleUrls: ['./zone.component.scss']
})
export class ZoneComponent implements OnInit {

    constructor(private socket: Socket,
                private game: GameStateService) {
    }

    ngOnInit() {
        this.socket.on('load zone', data => {
            console.log('Loading zone', data);

            const tiles = [];
            const zone = data.zone;
            this.game.character = data.character;

            zone.tiles.map(tile => {
                tiles[tile.position - 1] = tile;
            });
            zone.tiles = tiles;

            this.game.zone = zone;
        });
    }

    public click(tile) {
        this.socket.emit('click tile', tile);
    }

    public isZone() {
        return this.game.zone.type === 'zone';
    }

}
