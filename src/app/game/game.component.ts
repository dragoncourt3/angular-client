import {Component, OnInit} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {AuthService, GameStateService} from '@/_services';

@Component({
    selector: 'app-game',
    templateUrl: './game.component.html',
    styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

    constructor(private socket: Socket,
                private authService: AuthService,
                private game: GameStateService) {
    }

    ngOnInit() {
        this.socket.emit('load');
    }

    public get character() {
        return this.game.character;
    }

    public get zone() {
        return this.game.zone;
    }

}
