import {Component, OnInit} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {GameStateService} from '@/_services';

@Component({
    selector: 'app-game-encounter',
    templateUrl: './encounter.component.html',
    styleUrls: ['./encounter.component.scss']
})
export class EncounterComponent implements OnInit {

    constructor(private socket: Socket,
                private game: GameStateService) {
    }

    ngOnInit() {
        this.socket.on('load encounter', data => {
            console.log('Loading encounter', data);

            this.game.zone = data.zone;
            this.game.character = data.character;
        });

        this.socket.on('load death', data => {
            console.log('Character died', data);

            this.game.zone = data.zone;
            this.game.character = data.character;
        });
    }

    public isEncounter() {
        return this.game.zone.type === 'encounter';
    }

    public action(action) {
        console.log('emit encounter action');
        this.socket.emit('encounter action', action);
    }

}
