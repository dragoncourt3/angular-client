import {Component, OnInit} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {GameStateService} from '@/_services';

@Component({
    selector: 'app-game-character',
    templateUrl: './character.component.html',
    styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

    public backpack = [
        {
            name: 'Marks',
            quantity: 0,
        },
        {
            name: 'Rutter for Shangala',
            quantity: 4,
        },
        {
            name: 'Thief Insurance',
            quantity: 66,
        },
        {
            name: 'Camp Tent',
            quantity: 1,
        },
        {
            name: 'Cooking Gear',
            quantity: 1,
        },
        {
            name: 'Sleeping Bag',
            quantity: 1,
        },
        {
            name: 'Rope',
            quantity: 183,
        },
        {
            name: 'Rutter for Hie Brasil',
            quantity: 2,
        },
    ];

    constructor(private socket: Socket,
                private game: GameStateService) {
    }

    ngOnInit() {
        this.socket.on('load character', data => {
            console.log('Loading character', data);

            this.game.character = data.character;
            this.game.zone = {type: 'character'};

            // @todo remove this later
            this.backpack[0].quantity = data.character.marks;
        });
    }

    public isCharacter() {
        return this.game.zone.type === 'character';
    }

    public close() {
        this.socket.emit('reload zone');
    }

}
