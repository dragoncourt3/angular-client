import {Component, OnInit} from '@angular/core';
import {GameStateService} from '@/_services';

@Component({
    selector: 'app-game-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

    public actions: string[] = [];

    constructor(private game: GameStateService) {
    }

    ngOnInit() {
        this.game.characterListener.subscribe(character => this.actions = character.notifications || []);
    }

    public continue() {
        this.actions = [];
    }

}
